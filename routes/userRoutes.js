const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();
// TODO: Implement route controllers for user
// (get the user if it exist with entered credentials)
router.post(
  "",
  createUserValid,
  (req, res, next) => {
    try {
      res.data = UserService.add(req.body);
      res.status = 200;
    } catch (err) {
      console.log("UserRoutes.post catch err=", err);
      res.status = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
