const { Router } = require("express");
const AuthService = require("../services/authService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const bodyParser = require("body-parser");
const router = Router();

router.post(
  "/login",
  (req, res, next) => {
    try {
      // TODO: Implement login action
      // (get the user if it exist with entered credentials)

      res.data = AuthService.login(req.body);
      res.status = 200;
    } catch (err) {
      res.err = err;
      res.status = 400;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
