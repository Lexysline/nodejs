const { user } = require("../models/user");
const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation

  if (req.body.hasOwnProperty("id")) {
    throw 'Bad request. Request should not contain "id" field';
  }

  let errMsg = "";
  if (
    !req.body.hasOwnProperty("email") ||
    !req.body.hasOwnProperty("password") ||
    !req.body.hasOwnProperty("firstName") ||
    !req.body.hasOwnProperty("lastName") ||
    !req.body.hasOwnProperty("phoneNumber")
  ) {
    errMsg = "Bad request. Invalid emeil. Emeil should be gmail.com";
  }
  if (!req.body.email.toString().endsWith("@gmail.com")) {
    errMsg = "Bad request. Invalid emeil. Emeil should be gmail.com";
  }
  let pthoeReg = /^\+380[0-9]{9}$/g;
  if (!pthoeReg.test(req.body.phoneNumber.toString())) {
    errMsg = "Bad request. Invalid phone number format";
  }
  if (errMsg.length > 0) {
    throw errMsg;
  } else {
    let user = {};
    user.email = req.body.email;
    user.password = req.body.password;
    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    user.phoneNumber = req.body.phoneNumber;
    res.data = user;

    next();
  }
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  console.log("user.validation updateUserValid", req.body);
  next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
