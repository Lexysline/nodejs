const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user
  add(userData) {
    if (this.search(userData) == null) {
      console.log("UserService add: can be added");
      return UserRepository.create(userData);
    }
  }

  search(search) {
    const item = UserRepository.getOne(search);
    console.log("UserService search: ", item);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
